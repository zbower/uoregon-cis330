#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define ROCK 0
#define PAPER 1
#define SCISSOR 2

int main(){

	char userinput[12];
	srand(time(NULL));
	int userchoice;
	int validflag = 0 ; //init valid flag to false
	printf("Enter rock, paper, or scissor: ");
	
	while(!validflag){
	fgets(userinput , sizeof(userinput) , stdin);

	//string compare for input 
	if (strcmp(userinput ,"rock\n") == 0 ){
		validflag = 1;
		userchoice = ROCK;
		printf("You chose rock\n");
	} else if (strcmp(userinput ,"paper\n") == 0){
		validflag = 1; 
		userchoice = PAPER;
		printf("You chose paper\n");
	} else if (strcmp(userinput , "scissor\n") == 0){
		validflag = 1; 
		userchoice = SCISSOR;
		printf("You chose scissor\n");
	} else 
		printf("Input unrecognized, try again : ");
	}

	int compinput = rand() % 3 ;  

	switch(compinput){
		case ROCK:
			printf("Computer chose rock\n");
			if(userchoice == PAPER)
				printf("player wins , yay!\n");
			if(userchoice == ROCK)
				printf("it's a tie\n");
			if(userchoice == SCISSOR)
				printf("computer wins, too bad\n");break; 
		case PAPER:
			printf("Computer chose paper\n");
			if(userchoice == SCISSOR)
				printf("player wins , yay!\n");
			if(userchoice == PAPER)
				printf("it's a tie\n");
			if(userchoice == ROCK)
				printf("computer wins, too bad\n");break; 
		case SCISSOR:
			printf("Computer chose scissor\n");
			if(userchoice == ROCK)
				printf("player wins , yay!\n");
			if(userchoice == SCISSOR)
				printf("it's a tie\n");
			if(userchoice == PAPER)
				printf("computer wins, too bad\n");break; 
		default:
			printf("something went wrong");break; 
	}

	return 0;
}