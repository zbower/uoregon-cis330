#include <iostream>
#include "Game.h"


int main(void) {

    int row,col,steps;
    bool valid = false;
    std::string input;

    std::cout << "Please enter the size of the grid (int int): ";
    
    std::cin >> row >> col;
    while(std::cin.fail()){
        std::cout << "Invalid input, please enter two integers, with space in between " << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> row >> col;
    }   
    std::cout << "Please enter the number of steps: ";
    std::cin >> steps;
    
    while(std::cin.fail()){
        std::cout << "Invalid input, please enter two integers, with space in between " << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> steps;
    } 

    Game gametest(row ,col);
    std::cout << "\n/Initial State\n" << std::endl;
    gametest.PrintBoard();

    gametest.Step();
    int j = 1;
    while( j <= steps){
    std::cout << "\nBoard" << j << "\n" << std::endl;
    gametest.PrintBoard();
    gametest.Step();
    j++;
	}
    return 0;
}