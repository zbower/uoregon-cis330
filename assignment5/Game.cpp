#include "Game.h"
#include <ctime>
#include <cstdlib>
#include <iostream>


/*Note all I treat the board in a wrap around state
*/

Game::Game(int r, int c) {
    _row = r;
    _col = c;
    GenerateBoard();
    /*setting temporary grid  to game grid*/
    _gridcopy = _gamegrid;
    /*max farmer as 5%*/
    int total = _row * _col;
    //_maxf = .05 * total;
    //_maxw = .1 * total;
}

Game::Game(const Game &prevGame) {
    _row = prevGame._row;
    _col = prevGame._col;
    _gamegrid = prevGame._gamegrid;
}

void Game::Step(){
    int i,j;
        for(i=0; i < _row ; i++){  
            for(j = 0; j < _col; j++){
                int animal = _gamegrid[i][j];

                switch(animal){
                    case EMPTY:
                        CheckNeighborsEmpty(i,j);
                        break;
                    case SHEEP:
                        CheckNeighborsSheep(i,j);
                        break;
                    case WOLF:
                        CheckNeighborsWolf(i,j);
                        break;
                    case FARMER:
                        CheckNeighborsFarmer(i,j);
                        break;
                 }
            }
        }
        _gamegrid = _gridcopy;
    }

void Game::GenerateBoard() {
    srand(time(NULL));
    int i , j,r;
    int farmer = 0;
    int wolf = 0;
    int total = 4;
    for (i = 0; i < _row ; i++){
        /*create initial vector */
        std::vector<int>temp;
        for(j=0; j < _col; j++){
            r = (rand()%total);
            temp.push_back(r);
        }
        _gamegrid.push_back(temp);
    }
}

void Game::PrintBoard(){
    int i , j;
    for (i = 0; i < _row ; i++){
        for(j=0; j < _col; j++){
            int a = _gamegrid[i][j];
            switch(a){
                case EMPTY:
                std::cout << ". ";break;
                case SHEEP:
                std::cout << "S ";break;
                case FARMER:
                std::cout << "F ";break;
                case WOLF:
                std::cout << "W ";break;
            }
        }
        std::cout << std::endl;
    }
}

/*takes current position in board and checks neighborhs NOTE it wraps*/
void Game::CheckNeighborsEmpty(int x , int y){
    int i,j, animal;
    int sheep = 0;
    int wolf = 0;
    int farmer = 0;

    for (i= -1; i < 2; i++){
        for(j=-1; j < 2; j++){
            int row = (x + i + _row) % _row;
            int col = (y + j + _col) % _col; 
            animal = _gamegrid[row][col];
            if( animal == SHEEP){
                sheep++;
            } else if (animal == WOLF){
                wolf ++;
            } else if (animal == FARMER){
                farmer++;
            }
        }}
    if (sheep == 2){
        _gridcopy[x][y] = SHEEP;        
    } else if (wolf == 2){
        _gridcopy[x][y] = WOLF;
    } else if (farmer == 2){
        _gridcopy[x][y] = FARMER;
    }
}

/*takes current position in board and checks neighborhs NOTE it wraps*/
void Game::CheckNeighborsFarmer(int x , int y){
    int i,j;
    int animal;
    bool moved = false;
    for (i= -1; i < 2; i++){
        for(j=-1; j < 2; j++){
            int row = (x + i + _row) % _row;
            int col = (y + j + _col) % _col; 
            animal= _gamegrid[row][col];
            if(animal == EMPTY){
                int r = (rand()%2);
                if(r == 1){
                    _gridcopy[row][col] = FARMER;
                    _gridcopy[x][y] = EMPTY;
                    break;
                }
            }
        }
    }
}

void Game::CheckNeighborsWolf(int x , int y){
    int i,j, animal;
    int sheep = 0;
    int wolf = 0;
    int farmer = 0;

    for (i= -1; i < 2; i++){
        for(j=-1; j < 2; j++){
            int row = (x + i + _row) % _row;
            int col = (y + j + _col) % _col; 
            animal = _gamegrid[row][col];
        if( animal == SHEEP){
                sheep++;
            } else if (animal == WOLF){
                wolf ++;
            } else if (animal == FARMER){
                farmer++;
            }
        }}

        if (farmer > 0){
            _gridcopy[x][y] = EMPTY;
        } else if (wolf > 0 && sheep == 0){
            _gridcopy[x][y] == EMPTY;
        } else if (wolf > 3){
            _gridcopy[x][y] == EMPTY;
        }
}

void Game::CheckNeighborsSheep(int x , int y){
    int i,j, animal;
    int sheep = 0;
    int wolf = 0;
    int farmer = 0;

    for (i= -1; i < 2; i++){
        for(j=-1; j < 2; j++){
            int row = (x + i + _row) % _row;
            int col = (y + j + _col) % _col; 
            animal = _gamegrid[row][col];
            if( animal == SHEEP){
                sheep++;
            } else if (animal == WOLF){
                wolf ++;
            } else if (animal == FARMER){
                farmer++;
            }
}}
        if( sheep > 3){
            _gridcopy[x][y] == EMPTY;
        }
        if(wolf >= 1){
            _gridcopy[x][y] == EMPTY;
        }
}