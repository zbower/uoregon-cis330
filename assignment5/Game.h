#include <vector>

class Game{

public:

    Game(): _row(0), _col(0){}
    Game(int r , int c);

    /*copy constructor*/
    Game(const Game &prevGame);

    /*Go one step into the game state*/
    void Step();

    /*Updates the board*/
    void UpdateState();

    /*Generate Populatation*/
    void GenerateBoard();

    /*Debug function to print board*/
    void PrintBoard();

    /*check neighbor cells, originally wanted to 
    use pointers to inherited classes, but didn't have
    time to implement the way I wanted*/
    void CheckNeighborsFarmer(int x , int y);

    void CheckNeighborsEmpty(int x , int y);

    void CheckNeighborsWolf(int x , int y);

    void CheckNeighborsSheep(int x , int y);


private:

    enum _animals {EMPTY , SHEEP , WOLF , FARMER};

    /*Functions */
    void InitializeGrid();

    /*variables */
    int _row , _col;

    /*counts for population control, % of sheep,farmers ,etc*/
    int _scount, _wcount , _fcount;

    /*Change to 2d vector of pointers to the base class*/
    std::vector<std::vector<int> > _gamegrid;
    std::vector<std::vector<int> > _gridcopy;

    int _maxw,_maxf;
};
