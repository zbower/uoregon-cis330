#include <stdio.h>
#ifndef __MAZESOLVE_H_
#define __MAZESOLVE_H_

void AllocateArray(char*** array , int size );
void DeallocateArray(char *** array , int size);
void PopulateArray(char** array , int size, char* string); //maybe...
void PrintArray(char** array, int size);


int LocateStartingPos(char*** array, int size);
void StartMove(char*** array , int size, int startingMove);

void StartMaze(char***array , int row , int col, int direction, int size);



#endif