#include <stdio.h>
#include <stdlib.h>
#include "mazesolve.h"
#define PATH '.'

void AllocateArray(char*** array , int size ){
	/* Set up a pointer to elements , depending on the size var 
	taken in from maze.c, then iterate and allocate memory based on
	those pointers 
	input expected from file '@' , '.' , 'x'
	*/
	int i;
	*array = malloc(size * sizeof(char*));
	for(i=0; i < size ; i++){ 
		(*array)[i] = malloc(size * sizeof(char)); 
	}
}

void DeallocateArray(char*** array , int size){
	/* Frees the memory from allocate array */
	int i;
	for (i =0 ; i < size ; i++){
		free((*array)[i]);
	}
	free(*array);
}

int LocateStartingPos(char*** array, int size){
	//goes through the outside of square and finds
	//the starting pos, returns the value needed for first move
	//example, bottom row, first move = up
	int i;
	//BOTTOM CHECK
	for(i = 0; i < size ; i++){
		if ( (*array)[size-1][i] == 'x'){
			return 0;
			//return up for first move
			}
		}
	//TOP CHECK
	for(i=0 ; i < size; i++){
		if ( (*array)[0][i] == 'x'){
			return 1;
			//return down for first move
			}
		}
	//LEFT CHECK
	for (i =0 ; i < size ; i++){
		if ( (*array)[i][0] == 'x'){
			return 2;
			//return right for first move
			}
		}
	//RIGHT CHECK
	for (i = 0 ; i < size; i++){
		if ( (*array)[i][size-1] == 'x'){
			return 3;
			//return left for first move
			}
		}
	//for this project as we can only move up right 
	//left and down no need to worry about overlaps
	return -1; 
}


void StartMove(char*** array , int size, int startingMove){
	//0 up 1 down 2 right 3 left
	int i;
	switch(startingMove){
		//bottom row
		case 0: 
			for(i = 0; i < size ; i++){
				if ( (*array)[size-1][i] == 'x'){
							  //array row    col dir
					StartMaze(array , size-1 , i, 0,size);
					break;
					//keep hand on right wall
				//return up for first move
				}
			}
			break;
		//top row
		case 1:
			for(i=0 ; i < size; i++){
				if ( (*array)[0][i] == 'x'){
						//array row  col dir
				StartMaze(array , 0 , i, 1,size);
			break;
			}
		}
			break;
		//left col
		case 2:
			for (i =0 ; i < size ; i++){
				if ( (*array)[i][0] == 'x'){
						//array row  col dir
				StartMaze(array , i , 0, 2,size);
				break;
			//return right for first move
			}}
			break;
		//right col
		case 3:
			for (i =0 ; i < size ; i++){
				if ( (*array)[i][size-1] == 'x'){
						//array row  col dir
				StartMaze(array , i , 1, 3,size);
				break;
			//return right for first move
			}}
		break;
	}


}
/*00 01 02 03
  10 11 12 13
  20 21 22 23
  30 31 32 33
*/

//Takes a direction, then uses the right hand rule
//prioritizes like this : UP : RIGHT UP  LEFT DOWN
//						  DOWN:LEFT DOWN RIGHT UP
						//RIGHT:DOWN RIGHT UP LEFT
						//LEFT: UP LEFT DOWN RIGHT

//it gets to the end starting with maze three, and doesnt exit even though col is 0
//trying to

void StartMaze(char***array , int row , int col, int direction, int size){
	//flag for continuing program
	int exitFound = 0;
	//the direction we are looking, may change if need to 
	//turn ariound
	//int x , y; //vars to store the current pos
	while(!exitFound){
			//UP
			if(direction == 0){
				if ((*array)[row][col+1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("RIGHT\n");
					direction = 2; //right
					col++;
				}
				else if ((*array)[row-1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("UP\n");
					direction = 0; //up
					row--;
				} else if ((*array)[row][col-1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("LEFT\n");
					direction = 3; //left
					col --;
				} else if ((*array)[row +1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("DOWN\n");
					direction = 1;
					row ++;}
			}
			//DOWN
			if (direction == 1){
				if((*array)[row][col-1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("LEFT\n");
					direction = 3;//left
					col --;
				} else if((*array)[row+1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("DOWN\n");
					direction = 1; //down
					row++;
				} else if ((*array)[row][col+1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("RIGHT\n");
					direction = 2;//right
					col++;
				} else if((*array)[row -1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("UP\n");
					direction = 0; //up
					row--;
				}

			}
			//RIGHT
			if(direction == 2){
				if((*array)[row+1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("DOWN\n");
					direction = 1; //down
					row++;
				} else if((*array)[row][col+1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("RIGHT\n");
					direction = 2; //right
					col++;
				} else if((*array)[row-1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("UP\n");
					direction = 0; //UP
 					row--;
				} else if ((*array)[row][col-1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("LEFT\n");
					direction = 3; //left
					col --;
				}
			}
			//LEFT
			if(direction == 3){
				if((*array)[row-1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("UP\n");
					direction = 0; //up
					row --;
				} else if((*array)[row][col-1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("LEFT\n");
					direction = 3; //left
					col --;
				} else if ((*array)[row+1][col] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("DOWN\n");
					direction = 1; //down
					row++;
				} else if ((*array)[row][col+1] == PATH){
					//printf("row: %d, col: %d", row,col);
					printf("RIGHT\n");
					direction = 2; //right
					col++;
				}

			}
			//check if done, out of bounds
		if (row == 1 || col == 1){
			exitFound =1;
		}
		if (row == size -1 || col == size -1){
			exitFound = 1;
		}				

	}
}

//used for debugging
//
void PrintArray(char** array, int size){
	/* This takes a dynamic char 'array' and prints the character stored
	at the element */
	int i,j; 
	for(i = 0; i < size ; i++){
		for(j = 0; j < size ; j++){
			printf("%c", array[i][j]);
		}
		printf("\n");

	}
}
