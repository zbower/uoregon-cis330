#include <stdio.h>
#include <stdlib.h>
#include "mazesolve.h"

#define BUFFERSIZE 1024

enum Direction { UP , DOWN , RIGHT, LEFT };

int main(int argc , char*argv[]){
	
	if(argc != 2){
				//out    message formatted
		fprintf(stderr , "%s\n", "ERORR NO FILE");
		fprintf(stdout,"%s", "Error: No input file name\n");		
		return(1); //exit 		
	}

	FILE* file;

	file = fopen(argv[1] , "r");
	if(!file){
		printf("Unable to read file ");
		return(1);
	}	
		//def'd at top, to easily change size of buffer on needs
		char inputLine[BUFFERSIZE];
		int i, testCases,mazeSize; 
		char ** mazeArray;
		//setup input buffer and convert ints from file
		//from format testCases is first in file

		fgets(inputLine , BUFFERSIZE, file);
		testCases = atoi(inputLine);

		for(i=0; i < testCases; i++){
			//read from buffer and convert to int
			fgets(inputLine, BUFFERSIZE,file );

			//convert to int
			mazeSize = atoi(inputLine);

			printf( "ENTER\n" );

			//prepare array to be converted to line input
			AllocateArray(&mazeArray , mazeSize);
			
			/* go through and copy input buffer to */ 
			// send input buffer to the array line by line
			int k , p; 
			for(k = 0; k < mazeSize; k++){
				fgets(inputLine, BUFFERSIZE , file);
				for(p=0; p < mazeSize; p++)
					mazeArray[k][p] = inputLine[p];
			}
			int start = LocateStartingPos(&mazeArray, mazeSize);
			StartMove(&mazeArray , mazeSize, start);
			
			printf("\n");
			//PrintArray(mazeArray , mazeSize);
			DeallocateArray(&mazeArray,mazeSize);
			printf( "EXIT\n***\n" );
		}

		fflush(stdout);
		/* cleanup */
		fclose(file);
		//free(buffer);



		




return 0;	
}