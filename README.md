# CIS330 Repo
## This contains all the work for CIS330 at the University of Oregon

Author: Zachary Bower

Email: zbower@uoregon.edu

# Assignment 1

This is an assignemnt where we get familiar with git tools and test on ix-dev

git clone https://zbower@bitbucket.org/zbower/uoregon-cis330.git

# Assignment 2 

In this assignment we practice with pointers, allocating enough memory for a 2d array for various shapes and print them out. Also practice with make.  

Requirements: valgrind http://valgrind.org/  

git pull  

make  

make memcheck (This creates a binary of all the shapes and uses the tool valgrind)  



#variable to hold all files wish to compile
objs = src/main.c src/window/window.h src/window/window.c src/settings.h

#compiler option
cc = gcc
#compiler flags g(debug symbols)
cc_flags = -g -W -Wall

#linker flags LSDL2(sdl2)
ld_flags = -lSDL2

#name of binary to produce
bin_name = swag

#Targets
all : $(objs)
		$(CC) $(objs) $(cc_flags) $(ld_flags) -o $(bin_name)

#remove all objects
clean : 
	rm -f *~$(bin_name)


##################################

#Compiler flag
CC = g++
#linker flags LSDL2(sdl2)
LDFLAGS = -lSDL2 -lX11 -lGL -lGLU
#compiler flags
CCFLAGS = -g -Wall
#all object files needed
OBJECTS = GameLoop.o main.o Window.o

#g++ GameLoop.cpp GameLoop.h main.cpp sdlsrc/window.cpp sdlsrc/window.h -lSDL2 -lX11 -lGL -lGLEW

GameLoop.o: ./src/GameLoop.cpp ./src/GameLoop.h
	$(CC) -o GameLoop.o $^ 
Window.o: ./src/sdlsrc/window.cpp ./src/sdlsrc/window.h
	$(CC) -o Window.o  $^
main.o: ./src/main.cpp
	$(CC)  -o main.o $@ 

#name of binary to produce
BINNAME = swag

#Targets
all : $(OBJECTS)
		$(CC) $(OBJECTS) $(CCFLAGS) $(LDFLAGS) -o $(BINNAME)

#remove all objects
clean : 
	rm -f *~$(BINNAME) $(OBJECTS)


