#include <stdio.h>
#include <stdlib.h>
#include "triangle.h"

void allocateNumberTriangle(const int height, int ***triangle){
	int i; 
	int maxrows = (height * 2) -1; // thinking of a triangle as two squares, but filled different
	*triangle =  malloc (height * sizeof(int*));
	for (i =0; i < height; i++){
		(*triangle)[i] = malloc( maxrows * sizeof(int));
	}
}

//[f] --> [][] --> [maxrow]

void initializeNumberTriangle(const int height, int **triangle){
	int i, j;
	int maxrows = (height * 2) -1;
	for (i = 0; i < height; i++){
		for(j =0 ; j < maxrows; j++){
			triangle[i][j] = j;
			//printf("%d", triangle[i][j]);
		}
		//printf("\n");
	}
}

void printNumberTriangle(const int size, int **triangle){
	int i, j;
  	for (i = 0; i < size; i++) {
    	for (j = 0; j < size-i; j++) {
      		printf("  ");
    	}
    for (j = 0; j < (2*i + 1); j++) {
      	printf("%d ", triangle[i][j]);
    } 
    printf("\n");
	}
}
//freeing memory in reverse order it was allocated
void deallocateNumberTriangle(const int size, int ***triangle){
	int i; 
	//crashing at i =1
	for(i =0; i < size; i++){
		free((*triangle)[i]);
	}
	free(*triangle);
}