==24915== Memcheck, a memory error detector
==24915== Copyright (C) 2002-2015, and GNU GPL'd, by Julian Seward et al.
==24915== Using Valgrind-3.11.0 and LibVEX; rerun with -h for copyright info
==24915== Command: ./test-all.exe
==24915== 

Problem 1, Square:

Please enter the size of the Square [2-10]: 8
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 
0 1 2 3 4 5 6 7 

Problem 2 , Triangle:

Please enter the size of the Triangle [1-5]: 8
Invalid Input
Please enter the size of the Triangle [1-5]: 5
          0 
        0 1 2 
      0 1 2 3 4 
    0 1 2 3 4 5 6 
  0 1 2 3 4 5 6 7 8 

Problem 3 , Diamond:

Please enter the size of the Diamond [3-9]: 5
     0 
   0 1 2 
 0 1 2 3 4 
   0 1 2 
     0 
==24915== 
==24915== HEAP SUMMARY:
==24915==     in use at exit: 0 bytes in 0 blocks
==24915==   total heap usage: 23 allocs, 23 frees, 2,728 bytes allocated
==24915== 
==24915== All heap blocks were freed -- no leaks are possible
==24915== 
==24915== For counts of detected and suppressed errors, rerun with: -v
==24915== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
porke@tomford:~/Desktop/CIS330/uoregon-cis330/assignment2$ valgrind --leak-check=yes --track-origins=yes ./test-all.exe >> output.txt
==24921== Memcheck, a memory error detector
==24921== Copyright (C) 2002-2015, and GNU GPL'd, by Julian Seward et al.
==24921== Using Valgrind-3.11.0 and LibVEX; rerun with -h for copyright info
==24921== Command: ./test-all.exe
==24921== 


^C==24921== 
==24921== Process terminating with default action of signal 2 (SIGINT)
==24921==    at 0x4F31260: __read_nocancel (syscall-template.S:84)
==24921==    by 0x4EB45E7: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:592)
==24921==    by 0x4EB560D: _IO_default_uflow (genops.c:413)
==24921==    by 0x4E9625F: _IO_vfscanf (vfscanf.c:634)
==24921==    by 0x4EA55DE: __isoc99_scanf (isoc99_scanf.c:37)
==24921==    by 0x4007C1: GetUserInput (in /home/porke/Desktop/CIS330/uoregon-cis330/assignment2/test-all.exe)
==24921==    by 0x40085D: main (in /home/porke/Desktop/CIS330/uoregon-cis330/assignment2/test-all.exe)
==24921== 
==24921== HEAP SUMMARY:
==24921==     in use at exit: 0 bytes in 0 blocks
==24921==   total heap usage: 2 allocs, 2 frees, 5,120 bytes allocated
==24921== 
==24921== All heap blocks were freed -- no leaks are possible
==24921== 
==24921== For counts of detected and suppressed errors, rerun with: -v
==24921== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)

porke@tomford:~/Desktop/CIS330/uoregon-cis330/assignment2$ valgrind --leak-check=yes --track-origins=yes test-all.exe >> output.txt
valgrind: test-all.exe: command not found
porke@tomford:~/Desktop/CIS330/uoregon-cis330/assignment2$ valgrind --leak-check=yes --track-origins=yes ./test-all.exe
==24927== Memcheck, a memory error detector
==24927== Copyright (C) 2002-2015, and GNU GPL'd, by Julian Seward et al.
==24927== Using Valgrind-3.11.0 and LibVEX; rerun with -h for copyright info
==24927== Command: ./test-all.exe
==24927== 

Problem 1, Square:

Please enter the size of the Square [2-10]: 5
0 1 2 3 4 
0 1 2 3 4 
0 1 2 3 4 
0 1 2 3 4 
0 1 2 3 4 

Problem 2 , Triangle:

Please enter the size of the Triangle [1-5]: 5
          0 
        0 1 2 
      0 1 2 3 4 
    0 1 2 3 4 5 6 
  0 1 2 3 4 5 6 7 8 

Problem 3 , Diamond:

Please enter the size of the Diamond [3-9]: 5
     0 
   0 1 2 
 0 1 2 3 4 
   0 1 2 
     0 
==24927== 
==24927== HEAP SUMMARY:
==24927==     in use at exit: 0 bytes in 0 blocks
==24927==   total heap usage: 20 allocs, 20 frees, 2,548 bytes allocated
==24927== 
==24927== All heap blocks were freed -- no leaks are possible
==24927== 
==24927== For counts of detected and suppressed errors, rerun with: -v
==24927== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)