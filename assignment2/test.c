#include <stdio.h>
#include <stdlib.h>
#include "square.h"
#include "diamond.h"
#include "triangle.h"

#define MAX_SIZE_TRIANGLE 5
#define MIN_SIZE_TRIANGLE 1
#define MAX_SIZE_SQUARE 10
#define MIN_SIZE_SQUARE 2
#define MAX_SIZE_DIAMOND 9
#define MIN_SIZE_DIAMOND 3

int GetUserInput(char* shape , int min , int max){
	int integrityflag = 0; 
	int userSize;
	while(integrityflag !=1){
	printf("\nPlease enter the size of the %s from [%d-%d]: ", shape , min , max);
	//validate when scanf doent find an int
	if(scanf("%d" , &userSize)==0){
		char c;
  		while((c = getchar()) != '\n' && c != EOF) // clear input from non correct user input
    	;
	}
	if (userSize > max  || userSize < min) 
		printf("Invalid Input");
	else
		integrityflag = 1;	
	}
	return userSize;
}

int main(int argc , char**argv){

	printf("\nProblem 1, Square:\n");
	int userSize = GetUserInput("Square: ",MIN_SIZE_SQUARE, MAX_SIZE_SQUARE); 

	
	int ** arr;
	allocateNumberSquare(userSize , &arr);
	initializeNumberSquare(userSize,arr);
	printNumberSquare(userSize,arr);
	deallocateNumberSquare(userSize, &arr);

	printf("\nProblem 2 , Triangle:\n");
	
	int triangleSize = GetUserInput("Triangle: ", MIN_SIZE_TRIANGLE, MAX_SIZE_TRIANGLE);
	int** triangle;
	allocateNumberTriangle(triangleSize , &triangle);
	initializeNumberTriangle(triangleSize,triangle);
	printNumberTriangle(triangleSize, triangle);
	deallocateNumberTriangle(triangleSize, &triangle);
	
	printf("\nProblem 3 , Diamond:\n");
	int diamondSize = 2;

	while(diamondSize %2 ==0){
		diamondSize = GetUserInput("Diamond an odd number between: ",MIN_SIZE_DIAMOND, MAX_SIZE_DIAMOND);	
	}

	allocateNumberSquare(diamondSize , &arr);
	initializeNumberSquare(diamondSize,arr);
	printNumberDiamond(diamondSize, arr);
	deallocateNumberSquare(diamondSize, &arr);

	return 0;
}