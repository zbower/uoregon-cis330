#include <stdio.h>
#include <stdlib.h>
#include "diamond.h"

void printNumberDiamond(const int size, int **square){
	
	int i, j, k, l , m, n, p;
	p = size; //size for the 'offset'
	p--;
	for(k = 0; k < size; k+=2){
		for(n = 0; n < size - k; n++){
			printf(" ");
			//print spaces based on value of k, where at in the 'square'
		}
		for(l = 0; l <= k; l++){
			printf("%d ", square[k][l]);
		}
		printf("\n");
	}
		p -= 2; //only working with odd, would need to change if even values were allowed
	for(i = p; i >= 0; i-=2){
		for(m = 0; m < size - i; m++){
			printf(" ");
		}
	for(j = 0; j <= i; j++){
			printf("%d ", square[i][j]);
		}
		printf("\n");
	}
}