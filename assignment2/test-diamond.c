#include <stdio.h>
#include <stdlib.h>
#include "square.h"
#include "diamond.h"


#define MAX_SIZE 9
#define MIN_SIZE 3

int main(int argc , char**argv){

	printf("Problem 3, Diamond:\n");
	int integrityflag = 0; 
	int userSize; 

	while(integrityflag !=1){
	printf("\nPlease enter the size of the diamond [an odd number between 3 and 9 (inclusive)]: ");
	//validate when not a number.if 
	if(scanf("%d" , &userSize)==0){
		char c;
  		while((c = getchar()) != '\n' && c != EOF) // clear input from non correct user input
    	;
	}
	if (userSize > MAX_SIZE  || userSize < MIN_SIZE) 
		printf("Invalid input \n");
	else if(userSize %2 ==0){
		printf("Odd number please\n");
	}else
		integrityflag = 1;	
	}

	int ** arr;
	allocateNumberSquare(userSize , &arr);
	initializeNumberSquare(userSize,arr);
	printNumberDiamond(userSize, arr);
	deallocateNumberSquare(userSize, &arr);
	
	return 0;
}