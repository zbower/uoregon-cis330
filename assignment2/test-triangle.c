#include <stdio.h>
#include <stdlib.h>
#include "triangle.h"

#define MAX_SIZE 5
#define MIN_SIZE 1

int main(int argc , char**argv){

	printf("Problem 2, Triangle:\n");
	int integrityflag = 0; 
	int userSize; 

	while(integrityflag !=1){
	printf("Please enter the height of the triangle [1-5]:");
	//validate when not a number.if 
	if(scanf("%d" , &userSize)==0){
		char c;
  		while((c = getchar()) != '\n' && c != EOF) // clear input from non correct user input
    	;
	}
	if (userSize > MAX_SIZE  || userSize < MIN_SIZE) 
		printf("Invalid Input ");
	else
		integrityflag = 1;	
	}

	int ** arr;
	allocateNumberTriangle(userSize , &arr);
	initializeNumberTriangle(userSize,arr);
	printNumberTriangle(userSize, arr);
	deallocateNumberTriangle(userSize, &arr);
	
	return 0;
}