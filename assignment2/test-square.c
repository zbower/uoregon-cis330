#include <stdio.h>
#include <stdlib.h>
#include "square.h"

#define MAX_SIZE 10
#define MIN_SIZE 2

int main(int argc , char**argv){

	printf("Problem 1, Square:\n");
	int integrityflag = 0; 
	int userSize; 

	while(integrityflag !=1){
	printf("\nPlease enter the size of the square [2-10]: ");
	//validate when not a number.if 
	if(scanf("%d" , &userSize)==0){
		char c;
  		while((c = getchar()) != '\n' && c != EOF) // clear input from non correct user input
    	;
	}
	if (userSize > MAX_SIZE  || userSize < MIN_SIZE) 
		printf("Invalid Input");
	else
		integrityflag = 1;	
	}
	int ** arr;
	allocateNumberSquare(userSize , &arr);
	initializeNumberSquare(userSize,arr);
	printNumberSquare(userSize,arr);
	deallocateNumberSquare(userSize, &arr);
	
	return 0;
}