#include <stdio.h>
#include <stdlib.h>
#include "square.h"

void allocateNumberSquare(const int size, int ***square){
	int i; 
	*square = malloc(size * sizeof(int*)); //allocate for square "[][]"
	for(i = 0; i < size; i++){
		(*square)[i] = malloc(size * sizeof(int));
	}
}

void initializeNumberSquare(const int size, int **square){
	int i , j;
	for(i=0 ; i < size ; i++){
		for(j = 0; j < size ; j++){
			square[i][j] = j; 
		}
	}	
}

void printNumberSquare(const int size, int **square){
	int i , j;  
	for (i =0 ; i < size ; i++){
		for(j =0; j < size ; j++){
			printf("%d ", square[i][j]);
		}
		printf("\n");
	}
}

//freeing memory, but in the reverse order from allocate
void deallocateNumberSquare(const int size, int ***square){
	int i; 
	for (i =0 ; i < size ; i++){
		free((*square)[i]); //free col
	}	
	free(*square);
}


//[function]--> [row]--->[col] 



