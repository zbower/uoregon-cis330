//https://stackoverflow.com/questions/28911403/c-equivalent-of-c-fgets/28911594
//for c++ equivalent of fgets, and c++ ref for how to use

#include <iostream>
#include <fstream>
#include "maze.hpp"
#include "utils.hpp"

#define MIN_MAZE 10
#define MAX_MAZE 30

int main(int argc , char**argv){

    if( argc != 2 ){
        std::cerr << "Error: Input file needed " << std::endl;
        //return std error on no input file
        return 1;
    }
    //file to for reading input
    std::ifstream file (argv[1]);
    int testCases = 0;
    int row,col; //values to store current value of x
    //size_t temp;
    //get test cases from input file taken from argv
    file >> testCases;
    std::cout << "Test cases recieved: " << testCases << std::endl;
    for (int i = 0; i < testCases; i++ ) {
        int mazeSize = 0;
        file >> mazeSize;//get majze size to create maze obj

        std::cout << "Maze size is: " << mazeSize << std::endl;
        Utils mazeUtils(mazeSize, file);
        //Utils mazeUtils(mazeSize, mazeInputFile); // helper class for testing, output

        if (mazeSize < MIN_MAZE || mazeSize > MAX_MAZE) {
            std::cerr << "Error: parsing input file or size of mazes" << std::endl;
            std::cerr << "Check size of mazes should be <= " << MIN_MAZE << " and <= " << MAX_MAZE << std::endl;
            return 1;
            //exit condition if maze is not within the correct size
        }
        //create new maze obj, one for each test cases
        // Create a new maze object of the given size
        Maze maze(mazeSize);

        // Initialize the maze, set up row and col
        maze.readFromFile(file);
        while ( ! maze.atExit() ){
            ///get the x to print with maze utils
            maze.getCurrentPosition(row, col);
            //print the maze with the row and col gotten earlier
            mazeUtils.print(row, col); //a class to print
            std::cout << "Press enter to continue..." << std::endl;
            std::cin.get(); 
            //go through maze in 'steps'
            maze.step();
        } 
        maze.getCurrentPosition(row, col);
        mazeUtils.print(row, col);
        std::cout << "YAY! Maze solved!" << std::endl;   

    }
    return 0;
}


