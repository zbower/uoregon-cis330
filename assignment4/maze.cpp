#include <iostream>
#include "maze.hpp"
#include <string>
#define WALL '@'

//initialize vars to 0, and allocate array 
Maze::Maze(int size){
	this->size = size;
	numberOfMoves = 0;
	xStart =0;
	yStart = 0;
	row = 0;
	col = 0;
	currentDirection = RIGHT;
	mazeData = AllocateArray(mazeData);
}

//free allocated memory when object leaves scope
Maze::~Maze(){
	for (int i=0; i < size ; i++){
		delete [] mazeData[i];
	}
	delete[] mazeData;
}

//Allocates memory for the char[][] to be used,
char** Maze::AllocateArray(char** array){
	array = new char*[size];
	for(int i= 0; i < size ; i++){
		array[i] = new char[size];
	}
	return array;
}
/* reads lines from file stream, modeled after mazeutils, but using
char[][] instead of string*/
void Maze::readFromFile(std::ifstream &f){
	//variable to hold information from getline to populate array
	std::string line;
	getline(f,line); //getting gibberish first line, this just parses it and gets rid
	//modeled after the utils class
	for(int i= 0; i < size; i++){
		//read input line by line
		getline(f , line);
		for(int j = 0; j < size; j++){
			mazeData[i][j] = line[j];
			//while were here might as well check start pos
			if(line[j] == 'x'){
				row = i; //find row/col
				col = j;//for start pos
			}
		}
	}
	//PrintArray(row, col);
	//printf("xstart: %d yStart: %d\n", xStart,yStart );
}

//Get value in char[][]
char Maze::getValue(int x , int y){
	return mazeData[x][y];
}

//double check probably wrong
bool Maze::atEdge(int x , int y){
	if( ( y == 0 || y == ( size - 1 ) ) && ( x >= 0 && x <= ( size - 1 ) ) )
		return true;
	else if( ( x == 0 || x == ( size - 1 ) ) && ( y >= 0 && y <= ( size - 1 ) ) )
		return true;
	else
		return false;
}

//loops through the possible direction and chooses based on mod arth
//to make it cleaner, i adapted general rule for left hand maze
/*This left hand on wall algorithm can be simplified into these simple conditions:
- If you can turn left then go ahead and turn left,
- else if you can continue driving straight then drive straight,
- else if you can turn right then turn right.
- If you are at a dead end then turn around.
the overview of solving mazes is in part one
http://www.instructables.com/id/Maze-Solving-Robot/
of course changed for right hand */
void Maze::step(){
	//on init set to right, check all directions
	int direction = currentDirection;
	bool hasMoved = false; //condition for the "step"
	for (int i=0 ; i < 4; i++){
		if (hasMoved) //one move at a time, so we break if moved
			break;
		//go through 
		switch(direction){
			case DOWN: 
				if(isValidMove(row+1, col)){
					row++; //move down
					currentDirection = LEFT;
					hasMoved = true;
				}break;
			case RIGHT:
				if(isValidMove(row, col+1)){
					col++; //move right
					currentDirection = DOWN;
					hasMoved = true;
				}break;
			case UP:
				if(isValidMove(row-1, col)){
					row--;
					currentDirection = RIGHT;
					hasMoved = true;
				}break;
			case LEFT:
				if(isValidMove(row,col-1)){
					col--;//move left
					currentDirection = UP;
					hasMoved = true;
				}break;

		}
		direction++; //
		direction %=4;
	}
	numberOfMoves++;
	mazeData[row][col] = 'x';
	//PrintArray(row,col);
}

void Maze::getCurrentPosition(int &row, int &col){
	row = this->row; //set the values fo what was asked
	col = this->col;
}

bool Maze::atExit(){
	//was returning false sometimes, this stops it from exiting too early
	if (numberOfMoves < 1)
		return false;
	if (atEdge(row,col)){
		return true;}
	else
		return false;
}


bool Maze::isValidMove(int x, int y) {
	return ( x >= 0 && x <= ( size - 1 ) //bounds
			 && y >= 0 && y <= ( size - 1 )
			 && getValue(x,y) != WALL ); //check not wall
}

//function used originally to test, replaced by maze::utils
void Maze::PrintArray(int row , int col){
	mazeData[row][col]='x';
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			std::cout << mazeData[i][j]; 
		}
		std::cout << std::endl;
	}
}